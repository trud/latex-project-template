#! /bin/bash
# Fail the build if chktex finds issues.
if chktex -q $1 | grep '^'; then
  exit -1
fi
