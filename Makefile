# Project specific settings
DOCNAME = main
PDFNAME = main
OUTPUT_FOLDER = build

# LATEX tools
LATEXMK = latexmk
LATEXMK_FLAGS = -pdflatex=lualatex -pdf -jobname="$(OUTPUT_FOLDER)/$(PDFNAME)"

INDENT = latexindent
INDENT_FLAGS = -w -l -c=$(OUTPUT_FOLDER)
SRC=$(wildcard sections/*.tex)

CHKTEX = bash checktex.sh

LACHECK = lacheck

# Targets
all: doc
doc: pdf

indent:
	$(INDENT) $(INDENT_FLAGS) $(DOCNAME).tex

indentsec:
	for p in  $(SRC); \
	  do \
	  $(INDENT) $(INDENT_FLAGS) $$p ; \
	done

lacheck:
	$(LACHECK) $(DOCNAME).tex

chk:
	$(CHKTEX) $(DOCNAME).tex

pdf: indent indentsec lacheck chk
	$(LATEXMK) $(LATEXMK_FLAGS) $(DOCNAME).tex
	cp $(OUTPUT_FOLDER)/$(PDFNAME).pdf .
